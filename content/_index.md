---
title: Hector's Homepage
---

Hi, my name is Hector Escobedo. Welcome to my homepage.

The evolution of this website is long and storied. Too long of a story to fit
here, actually. Its primary function is simply to remind my friends and
acquaintances of my continued existence; there's also a list of things that I
find interesting.

Oh look, some [blog posts](/posts/) have recently appeared!

Feel free to [get in touch](/contact/) or perhaps just take a look at my
[résumé](/resume.pdf).

## My Interests

+ [Mathematics](/math/)
+ Computing
  - Free and open source software
  - Cryptography
  - Blockchain technology
  - Functional programming
      + Haskell
      + Elixir
      + Lisp
  - Formal verification
  - Distributed systems
  - Federated social media
+ Philosophy
  - Ethics
      + Meta-ethics
      + Applied ethics
      + Kant
      + Role morality
  - Philosophy of mathematics
  - Philosophy of technology
  - Education
  - Political philosophy
+ Economics
  - Behavioral economics
  - Finance
      + Bond pricing
      + Quantitative trading
      + Risk analysis
  - Market structures
  - Industrial organization
  - Theory of the firm
  - Monetary policy
  - Gift economies
+ Japanese culture
  - Anime
  - Go
  - Taiko
  - Film
  - Calligraphy
+ Writing
  - Typography
  - Digital publishing
  - Ergodic literature
  - Journalism
  - Stationery
