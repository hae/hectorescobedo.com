+++
title = "A solar leap week calendar without months"
date = '2022-03-13T23:00:00-07:00'
description = """
The common Gregorian calendar used worldwide has not been updated in over four
centuries. Here is an idea for calendar reform that respects the seven-day week
and drops the last vestiges of the lunar calendar by using annual quarters
instead of months.
"""
categories = [ "Time", "Space", "Math" ]
tags = [
"Time",
"Space",
"Math",
"mathematics",
"calendar",
]
+++

Since calendar reform efforts such as the [International Fixed
Calendar][IFC-wiki] have failed due to neglecting the religious significance of
the seven-day week, there seems to be growing consensus that for any perennial
calendar to be successful, it must use leap weeks instead of intercalary days
not assigned any day of the week. However, proposals such as [Hanke-Henry][HHPC]
and [Symmetry454][Sym454] retain the division of the year into twelve months
which are non-uniform in length. A calendar which is as regular as possible,
with uniform divisions of the year, is clearly superior: this was a major
advantage of the International Fixed Calendar. However, months are really a
vestige of ancient lunar calendars and do not fit neatly into the tropical year.

To avoid the dilemma of having either non-uniform months or thirteen months in
the year, which makes seasons a non-integral period of months, it is advisable
to get rid of them entirely. My proposed calendar instead uses the inverse
division of four quarters, corresponding to the temperate seasons, each with
exactly thirteen weeks. This is ideal for business and civil purposes, as
activities and reports can easily be planned to recur on a weekly, multi-weekly,
quarterly, or annual basis. A week remains seven days, and so the calendar will
continue to conform to religious considerations as well as customary work
schedules. For now, this proposal will be referred to as the Uniform Quarters
Perennial (UQP) calendar.

The UQP calendar may be constructed according to the following principles:
- Every week begins on Sunday and is exactly 7 days.
- Every quarter is exactly 13 weeks (91 days).
- Every common year is exactly 4 quarters (364 days).
- Leap weeks are appended to a common year when required, to maintain the start
  of the year as close as possible to the day of the Northern hemisphere vernal
  equinox, but not later. Therefore Easter will always occur in the first
  quarter, after 1 to 5 whole weeks.
- There will be an interval of 5 or 6 common years between leap weeks.

Dates will no longer require days of the week to be noted separately from days
of the month; for example, one can unambiguously refer to "Quarter 3, Week 12,
Friday" as a fixed date. Calendars can display a reusable 7-column, 13-row grid
design for each quarter without numbering the days, with one more row in the
last quarter for leap week.

![Quarter 4 calendar example](./calendar_quarter_4_example.png)

Since 20 March 2022 in the Gregorian calendar is a Sunday, and also marks the
vernal equinox, it is a perfect synchronization point for this reform. Thus it
will be designated as the first day of 2022 according to the UQP calendar, which
is a common year. Quarter 2 of 2022 will begin on Gregorian 19 June, Quarter 3
on 18 September, and Quarter 4 on 18 December. UQP 2023 will begin on Gregorian
19 March 2023. The UQP years 2027, 2032, and 2038 will end with leap weeks. As
an aside, this post is being published during the 2021 leap week! Also note that
the Symmetry454 leap rule is designed to keep the equinox at the same
_approximate_ date over the long term, not to ensure that it stays at or before
a certain date, and thus cannot be used for the UQP calendar. It has been
verified to output results that would put the start of the year after the
equinox. Further work is needed to determine precise algorithms for planning
leap weeks in this system.

[IFC-wiki]: https://en.wikipedia.org/wiki/International_Fixed_Calendar
[HHPC]: http://hankehenryontime.com/
[Sym454]: https://www.individual.utoronto.ca/kalendis/symmetry.htm
