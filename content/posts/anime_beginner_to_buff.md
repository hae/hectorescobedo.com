+++
title = "My journey from anime beginner to buff"
date = '2021-08-02T20:30:00-07:00'
lastmod = '2021-09-04T21:15:00-07:00'
draft = true
author = "Johan Bilderbund"
description = """
A reflection on my personal experiences with anime, from the first shows I \
watched to my present state of knowledge and involvement.
"""
categories = [ "Personal", "Japan" ]
tags = [
"Personal",
"Japan",
"Japanese culture",
"anime",
"animation",
"culture",
"television",
]
+++

If you already have a well-developed idea of "What is anime?", feel free to skip
the following introductory paragraph.

Even if you don't consider yourself an anime fan, there's a good chance you've
heard of it or maybe watched a handful of popular shows. The anime medium is a
subset of animated film and television series, specifically, those produced
primarily in Japan or for a Japanese audience. For decades, it has also
attracted a wider global following and influenced other forms of media in many
countries. The cross-cultural appeal of anime can be attributed in part to the
constant artistic innovation by its various creators as well as the medium's
accessibility to a wide range of age demographics. (That's right: anime is not
just for kids!)

At any rate, I am not ashamed to say that I'm a die-hard anime aficionado. Don't
worry, I'm not going to try and convince you here to start watching anime if you
haven't already. Neither is this an academic essay, but rather more of a
collection of personal reminiscences and an explanation of my journey so far in
appreciating great works of anime and how it eventually led me to explore other
aspects of Japanese culture. Anime has actually grown into a significant part of
my life over the past six years or so, and this could conceivably become the
first of a series of posts on the topic, or Japan in general.

## Starting out

Like many other Americans, my first foray into Japanese culture was watching
anime. I'm sure I watched _Pokémon_ as a young child, so technically that would
be the first show I ever saw, but I didn't actually know what anime was at that
point. It was always dubbed, and any Japanese cultural distinctions in the show
were minimized in order to appeal to an international audience. Therefore, to my
naïve mind, _Pokémon_ was more or less fully interchangeable with Western
cartoons like _Dexter's Laboratory_ or _Foster's Home for Imaginary Friends_,
although it was one of my favorites. It wouldn't be until high school that I
began to understand the appeal of anime in general and perceived it as a worthy
entertainment medium for adults.

My friend Erik introduced me to _Kill la Kill_ when we were between the 11th and
12th grades, so I was seventeen at the time.

One of the very first social events I attended as a college freshman was a
showing of the film _Patema Inverted_, sponsored by the local Anime Club, and it
completely blew my mind.

## The anime varsity club

Soon I began going to every regular Anime Club meeting. It became a fixture of
my weekly schedule.
