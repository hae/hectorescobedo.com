---
title: Hector's Math Corner
---

## Overview

I have a number of areas of interest in mathematics.

+ Abstract algebra
+ Graph theory
    - Ramsey theory
    - Dynamic networks
    - Matching algorithms
+ Number theory
+ Statistics
    - Markov chain models
    - Nonparametric statistics
+ Mathematical modeling
+ Fluid dynamics
+ Numerical analysis
+ Optimization problems
